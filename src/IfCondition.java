import java.util.Scanner;

public class IfCondition {
    public static void main(String[] args) {
        // Write a program to check if a candidate is eligible for voting or not. (Hint: Check age)
//        System.out.print("How old are you? ");
//        Scanner sc1 = new Scanner(System.in);
//        int age = sc1.nextInt();
//        if ( age >= 18){
//            System.out.println("You are eligible for voting!");
//        }else {
//            System.out.println("Sorry, You are not eligible for voting!");
//        }





        // Write a program to check if the number is positive or negative.
//        System.out.print("Please enter a number: ");
//        Scanner sc2 = new Scanner(System.in);
//        int num = sc2.nextInt();
//        if (num < 0){
//            System.out.println("negative");
//        }else {
//            System.out.println("positive");
//        }





        // Extend the previous program to check whether the given number is positive, zero or negative. (Hint: use if-else conditions)
//        System.out.print("Please enter a number: ");
//        Scanner sc3 = new Scanner(System.in);
//        int num = sc3.nextInt();
//        if (num < 0){
//            System.out.println("negative");
//        }else if (num == 0){
//            System.out.println("zero");
//        }else {
//            System.out.println("positive");
//        }




        // Write a program to find largest of two numbers.
//        int a = 3, b = 6;
//        if (a > b){
//            System.out.println("largest number is a = " + a);
//        }else {
//            System.out.println("largest number is b = " + b);
//        }




        // Write a program to check given number is even or odd. (Hint: use % operator)
        System.out.print("Please enter a number: ");
        Scanner sc4 = new Scanner(System.in);
        int num = sc4.nextInt();
        if ((num % 2) == 0){
            System.out.println(num + " is an even number.");
        }else {
            System.out.println(num + " is an odd number.");
        }
    }
}
