public class Main {

    public static void main(String[] args) {

        // Write programs to use all the data types and given arithmetic operations.
        int a = 1;
        double b = 2.5;
        double c = a + b;
        System.out.println("Result: " + c);

        char ch1 = 'a';
        char ch2 = 'b';
        System.out.println("char1: " + ch1);
        System.out.println("char2: " + ch2);
        System.out.println("char1 concatenate with char2: " + (ch1 + "" + ch2));

        boolean i = true;
        System.out.println("boolean: " + i);

        // Write program to perform all the arithmetic operations given in the table.
        System.out.println("Addition: " + a + " + " + b + " = " + (a + b));
        System.out.println("Subtraction: " + a + " - " + b + " = " + (a - b));
        System.out.println("Multiplication: " + a + " x " + b + " = " + (a * b));
        System.out.println("Division: " + a + " / " + b + " = " + (a / b));
        System.out.println("Increment of a=" + a + " is: " + ++a);
        System.out.println("Decrement of b=" + b + " is: " + --b);

    }
}
